#!/usr/bin/env python
# -*- coding: utf-8 -*-

# (c) 2021 Banco do Brasil
#
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

import copy
import json
import os
import pathlib
from urllib.parse import urljoin

from awx.main.credential_plugins.plugin import CredentialPlugin, CertFiles, raise_for_status
#import collections

import requests
from django.utils.translation import gettext_lazy as _

requests.urllib3.disable_warnings(requests.urllib3.exceptions.InsecureRequestWarning)

#CredentialPlugin = collections.namedtuple('CredentialPlugin', ['name', 'inputs', 'backend'])

inputs = {
    'fields': [
        {
            'id': 'account',
            'label': _('Account Name'),
            'type': 'string',
            'secret': False,
            'help_text': _('The user account to access the PIM API'),
        },
        {
            'id': 'url',
            'label': _('Server URL'),
            'type': 'string',
            'secret': False,
            'format': 'url',
            'help_text': _('The URL to the PIM API'),
        }
    ],
    'metadata': [
        {
            'id': 'identifier',
            'label': 'Identifier',
            'type': 'string',
            'help_text': 'The name of the key in My Credential System to fetch.'
        },
    ],
    'required': ['account','url']
}

certfile = None
token = None

cert = """-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA19mYDABYR3lkpR6o+SMUNC2efaLcf8KW2aMUj/rtPeVkRTec
cJIASJe6WxG24uvi89BdhzHoyMe4auzUFD13nRHP2bOehl/tMiQ/6/mspNbO8PH6
E+fymWLh3zoMMC3pt61VjjLqtMjPRGWOCRFmJ+xaaj+mJPwW01xeAHG9rMMiW4dI
JdpysUFH73CNBvmkA5wVDAoRTxmnnhDIz4YV3nIVfJFY9jSMA845mRa15TDqW/4v
Rxdhny9bMdyEPf2F2vRHmGu6u5UXPOmU/7bseM0hlY1nt1ULR55NupHPmvCmV/pP
js68KXsIyELuUylv/Eye8cvhlRiu148qwrBjaQIDAQABAoIBAFMXBBy6lUEOTdvy
LYGaK1DarsQWSWM0+PTWfjfelCzbfPnYKnR6/3jyT153NzurKjZI43gDkmSX3Tnr
6MfR0ZW5IZXgOwnnK1u98I1aE0dxGUBPJcE77Ht3QRO5OK/jPLDkHKccmpT1KDRg
E4kSFY0Qws1bqw1ZrT+3ssfhAO7biYSQccFEUfYplp1UFd7YCrDwZ5iR4GEZ70Lq
5ISQNLUPMiLwhuyAiTw9rd0M/Hc+Az0tCZlVbLfhljIfq7u0MMDYYXhUANdjHAxB
sBBmq8ZimMGGSKQ1LpwsmdXYebKb5R4o3MbvsqvygcyU28bvJsf8iZ4gzqY2pLxy
hOJnVfECgYEA/Wz75ca5/wCCHaAIVapPK8PVq4iwY7BeI9hhdXLMM6hsTW4zlviQ
zrDOgjqLGyIbGkOjCOkdxsTSGaPSWhXL/iJ7Vu93vujVly9F+V7kDiXq6/HdOjMY
ybUNQ9mJtse8YduAuezWPJXNRo1jsEr5hpimePof2NvoB8dTfRA2j8cCgYEA2grl
l4biPEz7RVqfgfoYKx5WlO8uuXuO5yCf8qRmf873TI9qMy9y5HHmFS9upgswK8u4
PBljXHQDknQLFFN2hzkXDfVoyhD5BY9D7zjYbW7JDXRJ/cH0OKEGSD2iV8oDw/VA
CfcVLdUjXstN4vNVuVbziW0S4XukjBSVcx3f008CgYEA8UuTjHRrJCGXwh1d0jMW
rtUH3QuI52JGQFsGcI2dYJI8jH0hId4KAsLV3algSbVvmNJNqW5Mq7U7Vq5k2uFm
GdM7HMtleKC4vfqE5asz+UShKYZg3clJEMUSnEfEmAQVmhclueGHGqiHi2WURYcd
jspJt2ZwbEr14nInTCzjxoECgYBcZrPHVBz5yvHNAUuJbG/jjfbJJNdbOVZk8jl2
PV9GmHHKmxg5WslIsE/9+Fzetu0SEkIgDe5CltOQs+aiFYCsa3AutSpUP38YcQ8s
CJ4HOiSvs/FLPW9+tBaSdLQgmfagi4U48+Ggy6syl9uI6vAQHB4WxgdzUM95oZt3
7lgVFwKBgGFKKVdggM74O5jIXEEfBkihikAY6T6KvROq9pLgoFMjp4t4F/pAndFU
AKQIGt050Z3OZAeBxPT576RRk8MtuqDPkHqOf3VlRbeZpzE+5CCaFPfCd/NwgFEE
FvN0+b0wBEtIEtCuArFIUl8JFlvT2df6NviZe4mKzgP7XLxl0ORA
-----END RSA PRIVATE KEY-----
-----BEGIN CERTIFICATE-----
MIIFqjCCBJKgAwIBAgICBBwwDQYJKoZIhvcNAQELBQAwbjELMAkGA1UEBhMCQlIx
HTAbBgNVBAoTFEJhbmNvIGRvIEJyYXNpbCBTLkEuMQ8wDQYDVQQLEwZJQ1AtQkIx
LzAtBgNVBAMTJkFDIEJhbmNvIGRvIEJyYXNpbCAtIFNFUlZJRE9SRVMgdjEgREVT
MB4XDTIwMDcyMTAzMDAwMFoXDTIzMDcyMTAyNTk1OVowgY4xCzAJBgNVBAYTAkJS
MRkwFwYDVQQIExBEaXN0cml0byBGZWRlcmFsMREwDwYDVQQHEwhCcmFzaWxpYTEd
MBsGA1UEChMUQmFuY28gZG8gQnJhc2lsIFMuQS4xDjAMBgNVBAsTBURJVEVDMSIw
IAYDVQQDExlvYWFzLXBraS5kZXNlbnYuYmIuY29tLmJyMIIBIjANBgkqhkiG9w0B
AQEFAAOCAQ8AMIIBCgKCAQEA19mYDABYR3lkpR6o+SMUNC2efaLcf8KW2aMUj/rt
PeVkRTeccJIASJe6WxG24uvi89BdhzHoyMe4auzUFD13nRHP2bOehl/tMiQ/6/ms
pNbO8PH6E+fymWLh3zoMMC3pt61VjjLqtMjPRGWOCRFmJ+xaaj+mJPwW01xeAHG9
rMMiW4dIJdpysUFH73CNBvmkA5wVDAoRTxmnnhDIz4YV3nIVfJFY9jSMA845mRa1
5TDqW/4vRxdhny9bMdyEPf2F2vRHmGu6u5UXPOmU/7bseM0hlY1nt1ULR55NupHP
mvCmV/pPjs68KXsIyELuUylv/Eye8cvhlRiu148qwrBjaQIDAQABo4ICLzCCAisw
JAYDVR0RBB0wG4IZb2Fhcy1wa2kuZGVzZW52LmJiLmNvbS5icjAOBgNVHQ8BAf8E
BAMCBLAwHQYDVR0lBBYwFAYIKwYBBQUHAwEGCCsGAQUFBwMCMIIBeQYDVR0fBIIB
cDCCAWwwgYeggYSggYGkfzB9MQswCQYDVQQGEwJCUjEdMBsGA1UEChMUQmFuY28g
ZG8gQnJhc2lsIFMuQS4xDzANBgNVBAsTBklDUC1CQjEvMC0GA1UEAxMmQUMgQmFu
Y28gZG8gQnJhc2lsIC0gU0VSVklET1JFUyB2MSBERVMxDTALBgNVBAMTBENSTDEw
MqAwoC6GLGh0dHA6Ly9wa2lkZXMuYmIuY29tLmJyL2FjaW50YTUvY3JsL0NSTDEu
Y3JsMIGroIGooIGlhoGibGRhcDovL2xkcDBwYTEuYmIuY29tLmJyOjM4OS9DTj1D
UkwxLENOPUFDJTIwQmFuY28lMjBkbyUyMEJyYXNpbCUyMC0lMjBTRVJWSURPUkVT
JTIwdjElMjBERVMsT1U9SUNQLUJCLE89QmFuY28lMjBkbyUyMEJyYXNpbCUyMFMu
QS4sQz1CUj9jZXJ0aWZpY2F0ZVJldm9jYXRpb25MaXN0MB0GA1UdDgQWBBSNGTSO
UdP8kwahQMoXefVb6dUyfDAfBgNVHSMEGDAWgBRESaekoeO8rBHCDy65vY/FjFu9
MTAXBgNVHSAEEDAOMAwGCisGAQQBsBgCAQEwDQYJKoZIhvcNAQELBQADggEBAFw2
bfWsBNww/jy+na90o7wveAMDhCGuGzGANHYnDoFqnoSyYRPtaCu1+JMYWdm08b3+
ZT+e/7cIl+HSX2pelzlkcEWNhao6WeSApCj3OjIazwW53Dx/7XfA7O3wQDm0JAuB
rAgZTnagHw1qSGhLI215LgQ1oiMAnX42KHoCwbhgodPN/ajbgukHurP5m2qSH9s0
0xEn4MPacXxWReGw9Sam6BbvAX26BAD+JjnLHkd4K65da8sYM5mhJx1+7VOk5VL/
6/QELNUb+1GP8bGvdGrxu3eSQqjxHzxwq0Is7wuILJ9prkaTXPhiCwXsqd5AuFnx
QnDHBbmt02boXz+zU58=
-----END CERTIFICATE-----"""

def get_token(**kwargs):
    pim_url = kwargs['url'] + "/ERPMWebService/AuthService.svc/REST" + "/Login"
    
    #pim_cert = os.getenv('RUN_GLOBAL_VAULT_OAAS_PIM')
    #pim_cert = kwargs('certificate')
    
    data = { 
        "LoginType": 4
    }

    #cert_path = open('/tmp/pim_cert.pem', 'w')
    #cert_path.write(pim_cert)
    #cert_path.close()

    with CertFiles(cert) as certfile:

      with requests.Session() as session:
        #session.cert = '/tmp/pim_cert.pem'
        session.cert = certfile
        session.verify = False

        try:
            req = requests.Request('POST', pim_url, data=json.dumps(data))
            preppared = req.prepare()
            preppared.headers['Content-Type'] = 'text/json'
            response = session.send(preppared)
            token = response.json()        
            
            return token

        except Exception as err:
            raise ValueError(err)



def release_token(**kwargs):
    pim_url = kwargs['url'] + "/ERPMWebService/AuthService.svc/REST" + "/StoredCredential"
    
    user = kwargs['account']

    headers = {
        "User-Agent": "sgh-awx-pimweb-plugin/0.0.1",
        "Accept": "*/*",
        "Accept-Encoding": "gzip, deflate, br",
        "Connection": "keep-alive"
    }

    data= {
        "AuthenticationToken": token,
        "Comment": "AWX",
        "AccountIdentificationInfo": {
            "AccountName": user,
            "AccountStore": {
                "TargetName": "LDP0PB1.BB.COM.BR\\SERVICE_USERS",
                "Type": 15
            }
        }
    }

    #pim_cert = kwargs('certificate')

    with CertFiles(cert) as certfile:

      with requests.Session() as session:
        #session.cert = '/tmp/pim_cert.pem'
        session.cert = certfile
        session.verify = False

        try:
            req = requests.Request('PUT', pim_url, data=json.dumps(data), headers=headers)
            
            preppared = req.prepare()
            preppared.headers['Content-Type'] = 'text/json'

            response = session.send(preppared)

            return response.content

        except Exception as err:
            raise ValueError(err)



def pimweb_backend(**kwargs):
    pim_url = kwargs['url'] + "/ERPMWebService/AuthService.svc/REST" + "/StoredCredential"
    user = kwargs['account']
    identifier = kwargs['identifier']

    token = get_token(**kwargs)

    if not token:
        raise ValueError('Token error!')

    params = {
        "SystemType": "Directory_IBM_Tivoli",
        "SystemName": "LDP0PB1.BB.COM.BR\SERVICE_USERS",
        "CustomTypeName": "[LDAP]",
        "AccountName": user,
        "Comment": "AWX"
    }

    headers = {
        "User-Agent": "sgh-awx-pimweb-plugin/0.0.1",
        "Accept": "*/*",
        "Accept-Encoding": "gzip, deflate, br",
        "Connection": "keep-alive",
        "AuthenticationToken": token
    }

    #pim_cert = kwargs('certificate')

    with CertFiles(cert) as certfile:

      with requests.Session() as session:
        #session.cert = '/tmp/pim_cert.pem'
        session.cert = certfile
        session.verify = False

        try:
            req = requests.Request('GET', pim_url, headers=headers, params=params)
            
            preppared = req.prepare()
            preppared.headers['Content-Type'] = 'text/json'

            response = session.send(preppared)
            resp = response.json()

            release_token(**kwargs)

            if identifier in resp:
                #raise_for_status(resp)
                return resp[identifier]

        except Exception as err:
            raise ValueError(err)


pimweb_plugin = CredentialPlugin('PIMWEB', inputs=inputs, backend=pimweb_backend)
