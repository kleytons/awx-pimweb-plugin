#!/usr/bin/env python3

from setuptools import setup

requirements = []

setup(
    name='awx-pimweb',
    version='0.0.1',
    author='C1299770 Kleyton Nogueira Santos',
    author_email='c1299770@bb.com.br',
    description='',
    long_description='',
    license='Apache License 2.0',
    keywords='ansible',
    url='https://fontes.intranet.bb.com.br/sgh/sgh-awx-pimweb-plugin',
    packages=['awx_pimweb'],
    include_package_data=True,
    zip_safe=False,
    setup_requires=[],
    install_requires=requirements,
    entry_points = {
        'awx.credential_plugins': [
            'pimweb_plugin = awx_pimweb:pimweb_plugin',
        ]
    }
)
